import axios from 'axios';

const AUTH_TOKEN = 'Token 0600292623824b5aecb79a2d860a86046a97c6d9';
const instance = axios.create({
	baseURL: 'http://127.0.0.1:8000/api/',
});

instance.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// instance.defaults.headers.post['Content-Type'] = 'application/json';
instance.defaults.headers.common['Content-Type'] = 'application/json';
export default instance;
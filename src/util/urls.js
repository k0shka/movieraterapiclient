base_url = 'http://127.0.0.1:8000/api/'
// movies 
export const GET_MOVIES = '';
export let GET_MOVIE = ``;
export let ADD_MOVIE = ``;
export let EDIT_MOVIE = ``;
export let DELETE_MOVIE = ``;
export let RATE_MOVIE = ``;
// ratings 
export const GET_RATINGS = '';
export let DELETE_RATING = ``;

// auth 
export const OBTAIN_TOKEN = '';
export const REGISTER_USER = '';
